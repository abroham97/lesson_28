#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Some noise\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Wo-of!\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Me-ow!\n";
	}
};

class Horse :public  Animal
{
public:
	void Voice() override
	{
		std::cout << "I-go-go!\n";
	}
};

class Mouse :public  Animal
{
public:
	void Voice() override
	{
		std::cout << "Pi-pi!\n";
	}
};

class Cow :public  Animal
{
public:
	void Voice() override
	{
		std::cout << "Mu-u-u-u!\n";
	}
};

int main()
{
	int num = 5;
	Animal** animals = new Animal * [num]
	{
		new Dog(),
			new Cat(),
			new Horse(),
			new Mouse(),
			new Cow()
	};

	for (int i = 0; i < num; i++)
	{
		animals[i]->Voice();
	}
}
